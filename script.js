/*Теоретичне питання
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Javascript побудована на обьектах які мають свої властивості та методи. Коли потрібно звернутись до якоїсь властивості чи методу Javascript починає їх шукати в самому обьекті, але коли не знаходить, тоді звертається до властивостей його прототипів і шукає там.


Для чого потрібно викликати super() у конструкторі класу-нащадка?

super() у конструкторі класу-нащадка потрібен для того, щоб була можливість звертатись до властивостей які знаходяться у конструкторі обькта якому створюється нащадок.

*/

// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {

    constructor(name, age, salary){
        this._name = name,
        this._age = age,
        this._salary = salary
    }


    set name (newName) {
        this._name = newName;
    }

    set age (newAge) {

        if(newAge > 0 && newAge <= 100){
            this._age = newAge
        }
      
    }

    set salary (newSalary) {

        if(newSalary > 0 && newSalary <= 1000000){
            this._salary = newSalary
        }
      
    }

    get name () {
        return this._name;
    }

    get age () {
        return this._age;
    }

    get salary () {
        return this._salary;
    }



}

class Programmer extends Employee {

    constructor (name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }

    get salary (){
        return this._salary * 3;
    }
}

const a = new Programmer('Alex', 18, 100, 'En');
const b = new Programmer('Max', 20, 400, 'En');
const c = new Programmer('Elen', 30, 1000, 'En');

console.log(a, b, c);